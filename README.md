# Hudl Test #

Overview:
Android test for Hudl. This is one Activity app that displays the map and pulls data from Eventful website. 
Data is displayed on the map. Data represent a sport event in the current week and it is based by the user location or position of the map camera. Every event has a Title and date.

Application use 3rd party libraries like Retrofit to handle REST calls and access 
Eventful API. Uses Gson to convert data to readable data model. 
Apart of that there are google libraries to use the map and access user location. 

Interactions:
-Refresh button is part of the interface. Allows to move the map back to the original (current) user location.
 Clears the map and pulls the new data from that location. 

-Move map allows user to travers the map to different city and app will pull data from that city and displays it on 
 the map. It is recommended to move to big cities like London, Barcelona, Madrid, Paris etc. becasue Eventful doesn't 
 have much data for small cities. 

Limitations:
- No data to display: there might a situation where there is no events for current week

- There are no filters to change date range for events, it might be updaed later 

- Multiple events in the same location - need to implement Pin shuffle 

- Currently app is able to display 100 events for the same location, this should be fixed in 
  next version. For example New York might have 300 events but only 100 will be displayed