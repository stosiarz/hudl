package com.kamilstosik.hudl.permissions;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Handle location permissions and if necessary add other permissions as well
 * Created by Kamil on 5/21/2017.
 */

public class PermissionManager {
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    private static boolean mLocationPermissionGranted = false;

    public static boolean checkLocationPermissionIsGranted(Activity activity) {
         /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(activity,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(activity,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

        return mLocationPermissionGranted;
    }

    public static void setLocationPermissionGranted(boolean locationPermissionGranted) {
        mLocationPermissionGranted = locationPermissionGranted;
    }

    public static boolean isLocationPermissionGranted(Activity activity) {
        checkLocationPermissionIsGranted(activity);
        return mLocationPermissionGranted;
    }
}
