package com.kamilstosik.hudl.retrofit;

import com.kamilstosik.hudl.data.EventfulData;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Retrofit service
 * Created by Kamil on 5/18/2017.
 */
public class RetrofitService {
    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(EventfulData.API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    /**
     * Create retrofit service
     * @param serviceClass
     * @param <S>
     * @return
     */
    public static <S> S createService(Class<S> serviceClass) {
        OkHttpClient client = httpClient.build();
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }
}
