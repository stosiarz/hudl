package com.kamilstosik.hudl.retrofit;

import com.kamilstosik.hudl.data.EventsHolder;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.kamilstosik.hudl.data.EventfulData.API_SEARCH;

/**
 * Eventful interface to access API
 * Created by Kamil on 5/18/2017.
 */
public interface EventfulClient {
    @GET(API_SEARCH)
    Call<EventsHolder> searchEvents(@Query("app_key") String app_key,
                                    @Query("location") String location,
                                    @Query("date") String date,
                                    @Query("category") String category,
                                    @Query("page_size") int page_size);
}
