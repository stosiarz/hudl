package com.kamilstosik.hudl.retrofit;

import android.text.TextUtils;

import com.kamilstosik.hudl.data.Event;
import com.kamilstosik.hudl.data.EventfulData;
import com.kamilstosik.hudl.data.Events;
import com.kamilstosik.hudl.data.EventsHolder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Access Retrofit to pull data of Eventful
 * Created by Kamil on 5/21/2017.
 */
public class RetrofitManager {

    private List<Event> mEventsList;
    private static RetrofitManager mInstance;

    public static RetrofitManager getInstance() {
        if(mInstance==null) {
            mInstance = new RetrofitManager();
        }
        return mInstance;
    }

    private RetrofitManager() {}

    /**
     * Set retrofit to pull Eventful data
     * @param city
     * @param callback
     */
    public void pullEventData(String city, EventfulCallback callback) {
        if(!TextUtils.isEmpty(city)) {
            mEventsList = new ArrayList<>();
            EventfulClient client = RetrofitService.createService(EventfulClient.class);
            // Fetch list of sports event in the given location for whole week and limit results to 100
            // later This week could be replaced by filter variable set by user
            // and amount of events should be lower and retrofit should keep calling page by page to get them all
            Call<EventsHolder> call = client.searchEvents(EventfulData.APP_KEY, city, "This Week", "sports", 100);
            doCall(call, callback);
        }
    }

    /**
     * Perform a call to API
     * @param call
     * @param callback
     */
    private void doCall(Call<EventsHolder> call, final EventfulCallback callback) {
        call.enqueue(new Callback<EventsHolder>() {
            @Override
            public void onResponse(Call<EventsHolder> call, Response<EventsHolder> response) {
                EventsHolder eventsHolder = response.body();
                if(eventsHolder!=null) {
                    Events events = eventsHolder.getEvents();
                    if(events!=null) {
                        List<Event> listEvents = events.getEvent();
                        if(listEvents!=null && listEvents.size()>0) {
                            for (Event e : listEvents) {
                                mEventsList.add(e);
                            }
                            callback.finishCall(mEventsList);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<EventsHolder> call, Throwable t) {
               // Log.e("test", "retrofit failed, "+t.getMessage());
            }
        });
    }
}
