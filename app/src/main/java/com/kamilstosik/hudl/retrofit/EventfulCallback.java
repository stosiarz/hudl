package com.kamilstosik.hudl.retrofit;

import com.kamilstosik.hudl.data.Event;

import java.util.List;

/**
 * Pass list of event after call is finished and processed
 * Created by Kamil on 5/21/2017.
 */
public interface EventfulCallback {
    void finishCall(List<Event> events);
}
