package com.kamilstosik.hudl.map;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.text.TextUtils;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kamilstosik.hudl.data.Event;
import com.kamilstosik.hudl.permissions.PermissionManager;
import com.kamilstosik.hudl.retrofit.EventfulCallback;
import com.kamilstosik.hudl.retrofit.RetrofitManager;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Singleton class to perform all the map related tasks
 * Created by Kamil on 5/20/2017.
 */

public class MapManager {
    //set map with default zoom
    private static final int DEFAULT_ZOOM = 12;
    // Default location set in Sydney in case current location can't be retrieved
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);
    //save last city where data were pulled from
    private String mCurrentCity;
    private Geocoder mGeocoder;

    private static MapManager mInstance;

    /**
     * Get singleton of a map manager
     * @return
     */
    public static MapManager getInstance() {
        if(mInstance==null) {
            mInstance = new MapManager();
        }
        return mInstance;
    }

    private MapManager(){}

    /**
     * Gets the current location of the device, and positions the map's camera.
     * @param activity
     * @param map
     * @param googleApi
     */
    public void setupMapInCurrentLocation(Activity activity, GoogleMap map, GoogleApiClient googleApi, EventfulCallback callback) {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        Location currentLocation = getCurrentLocationFromApi(activity, googleApi);
        // Set the map's camera position to the current location of the device.
        if (currentLocation != null) {
            LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(currentLocation.getLatitude(),
                               currentLocation.getLongitude()),
                               DEFAULT_ZOOM));

            String city = geocodeLocationToExtractCity(getAndInitGeocoder(activity.getApplicationContext()), latLng);
            if(!TextUtils.isEmpty(city)) {
                RetrofitManager.getInstance().pullEventData(city, callback);
                mCurrentCity = city;
            }
        } else {
            //move to deafault location in case current location can't be retrieved
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
            map.getUiSettings().setMyLocationButtonEnabled(false);
        }
    }

    /**
     * Add camera listener on idle, used to pull event data after user move the map
     * @param map
     * @param listener
     */
    public void setCameraIdleListener(GoogleMap map, GoogleMap.OnCameraIdleListener listener) {
        map.setOnCameraIdleListener(listener);
    }

    /**
     * Transform lat lng into readable address, in this case return only the city
     * @param geocoder
     * @param position
     * @return
     */
    public String geocodeLocationToExtractCity(Geocoder geocoder, LatLng position) {
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(position.latitude, position.longitude, 1);
            if(addresses!=null && addresses.size()>0) {
                String city = addresses.get(0).getLocality();
                if (!TextUtils.isEmpty(city)) {
                    return city;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Retrieve current location from google api
     * @param activity
     * @param googleApiClient
     * @return
     * @throws SecurityException
     */
    public Location getCurrentLocationFromApi(Activity activity, GoogleApiClient googleApiClient) throws SecurityException{
        if (PermissionManager.isLocationPermissionGranted(activity)) {
            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            return location;
        }
        return null;
    }

    /**
     * Update map with events retrieved from Eventful api
     * @param map
     * @param events
     */
    public void updateMapWithEvents(GoogleMap map, List<Event> events) {
        if(map!=null && events!=null && events.size()>0) {
            map.clear();
            for(Event e : events) {
                LatLng sydney = new LatLng(e.getLat(), e.getLng());
                map.addMarker(new MarkerOptions().position(sydney).title(e.getTitle()).snippet(e.getStartTime()));
            }
        }
    }

    public Geocoder getAndInitGeocoder(Context context) {
        if(mGeocoder==null) {
            mGeocoder = new Geocoder(context, Locale.getDefault());
        }
        return mGeocoder;
    }

    public String getCurrentCity() {
        return mCurrentCity;
    }

    public void setCurrentCity(String city) {
        mCurrentCity = city;
    }
}
