package com.kamilstosik.hudl.data;

/**
 * Static strings
 * Created by Kamil on 5/18/2017.
 */

public class EventfulData {
    public static final String APP_KEY = "FBrpCHG7JNFTn6FW";
    public static final String API_BASE_URL = "http://api.eventful.com/";
    public static final String API_SEARCH = "/json/events/search";
}
