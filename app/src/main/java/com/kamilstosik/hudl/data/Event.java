package com.kamilstosik.hudl.data;

/**
 * Event model with all it's data
 * Currently I' don't use all the information
 * therefore only few have getters
 * Created by Kamil on 5/18/2017.
 */

public class Event {
    private String id;
    private String url;
    private String title;
    private String description;
    private String start_time;
    private String stop_time;
    private String venue_id;
    private String venue_url;
    private String venue_name;
    private String venue_display;
    private String venue_address;
    private String city_name;
    private String region_name;
    private String region_abbr;
    private String postal_code;
    private String country_name;
    private int all_day;
    private double latitude;
    private double longitude;
    private String geocode_type;
    private int trackback_count;
    private int calendar_count;
    private int comment_count;
    private int link_count;
    private String created;
    private String owner;
    private String modified;

    public Event() {
    }

    public String getId() {
        return id ;
    }

    public String getUrl() {
        return url ;
    }

    public String getTitle() {
        return title ;
    }

    public String getStartTime() {
        return start_time ;
    }

    public double getLat() {
        return latitude ;
    }

    public double getLng() {
        return longitude ;
    }

    public void setLat(double lat) {
        latitude = lat;
    }

    public void setLng(double lng) {
        longitude = lng;
    }
}
