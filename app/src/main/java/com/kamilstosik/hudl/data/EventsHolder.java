package com.kamilstosik.hudl.data;

/**
 * Search json from Eventful
 * http://api.eventful.com/docs/events/search
 * Created by Kamil on 5/17/2017.
 */
public class EventsHolder {

    private int total_items ;
    private int page_size ;
    private int page_count;
    private int page_number;
    private int page_items;
    private int first_item;
    private int last_item;
    private float search_time;
    private Events events;

    public EventsHolder() {
    }

    public int getTotalItems() {
        return total_items ;
    }

    public int getPageSize() {
        return page_size ;
    }

    public Events getEvents() {
        return events ;
    }
}