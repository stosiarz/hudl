package com.kamilstosik.hudl.data;

import java.util.List;

/**
 * List of events JSON model
 * Created by Kamil on 5/18/2017.
 */

public class Events {
    private List<Event> event = null;

    public List<Event> getEvent() {
        return event;
    }
}
