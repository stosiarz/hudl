package com.kamilstosik.hudl;

import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.location.places.Places;
import com.kamilstosik.hudl.data.Event;
import com.kamilstosik.hudl.map.MapManager;
import com.kamilstosik.hudl.permissions.PermissionManager;
import com.kamilstosik.hudl.retrofit.EventfulCallback;
import com.kamilstosik.hudl.retrofit.RetrofitManager;

import java.util.List;

import static com.kamilstosik.hudl.permissions.PermissionManager.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION;

/**
 * Main activity, entry point and map fragment display
 */
public class MainActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        EventfulCallback {

    private Button mRefreshBtn;
    private TextView mCityText;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        mRefreshBtn = (Button) findViewById(R.id.refresh_btn);
        mRefreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveTocurrentLocation();
            }
        });
        mCityText = (TextView) findViewById(R.id.city_text);
        startGoogleApi();
    }

    private void startGoogleApi() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */,
                        this /* OnConnectionFailedListener */)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();

        mGoogleApiClient.connect();
    }

    /**
     * Set the map in current location and pull initial events and update city name
     */
    private void moveTocurrentLocation() {
        MapManager.getInstance().setupMapInCurrentLocation(MainActivity.this,
                mMap,
                mGoogleApiClient,
                MainActivity.this);
        String city = MapManager.getInstance().getCurrentCity();
        if(!TextUtils.isEmpty(city)) {
            mCityText.setText(city);
        }
    }

    /**
     * Listener for the map that updates the event after map is moved
     */
    private GoogleMap.OnCameraIdleListener idleCameraListener = new GoogleMap.OnCameraIdleListener() {
        @Override
        public void onCameraIdle() {
            CameraPosition cameraPos = mMap.getCameraPosition();
            LatLng pos = cameraPos.target;
            Geocoder geocoder = MapManager.getInstance().getAndInitGeocoder(getApplicationContext());
            String city = MapManager.getInstance().geocodeLocationToExtractCity(geocoder, pos);
            String currentCity = MapManager.getInstance().getCurrentCity();
            if(!TextUtils.isEmpty(city) && !TextUtils.isEmpty(currentCity) && !currentCity.contentEquals(city)) {
                RetrofitManager.getInstance().pullEventData(city, MainActivity.this);
                mCityText.setText(city);
                MapManager.getInstance().setCurrentCity(city);
            }
        }
    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        MapManager.getInstance().setCameraIdleListener(googleMap, idleCameraListener);
        moveTocurrentLocation();
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    PermissionManager.setLocationPermissionGranted(true);
                    moveTocurrentLocation();
                }
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Log.d("test", "Play services connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //Log.d("test", "Play services connection failed: ConnectionResult.getErrorCode() = "+ connectionResult.getErrorCode());
    }

    @Override
    public void finishCall(List<Event> events) {
        MapManager.getInstance().updateMapWithEvents(mMap, events);
    }
}
