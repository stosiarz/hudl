package com.kamilstosik.hudl;

import com.kamilstosik.hudl.data.Event;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test Event model
 * Created by Kamil on 5/21/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class EventTest {

    @Test
    public void verifyEvent() {
        // create and configure mock
        Event event = mock(Event.class);
        when(event.getLat()).thenReturn(43.345);
        // call method testing on the mock with parameter 43.345
        event.setLat(43.345);
        event.getLat();
        // now check if method testing was called with the parameter 43.345
        verify(event).setLat(Matchers.eq(43.345));

        when(event.getLng()).thenReturn(-11.345);
        event.setLat(-11.345);
        event.getLat();
        verify(event).setLat(Matchers.eq(-11.345));
    }
}
