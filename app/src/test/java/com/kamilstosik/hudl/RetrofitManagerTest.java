package com.kamilstosik.hudl;

import com.kamilstosik.hudl.retrofit.EventfulCallback;
import com.kamilstosik.hudl.retrofit.RetrofitManager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Test RetrofitManager
 * Created by Kamil on 5/21/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class RetrofitManagerTest {

    @Mock
    EventfulCallback mMockEventfullCallback;


    @Test
    public void pullEventDataTest() {
        RetrofitManager.getInstance().pullEventData("London", mMockEventfullCallback);
    }
}
