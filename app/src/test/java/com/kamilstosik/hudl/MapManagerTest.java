package com.kamilstosik.hudl;

import android.content.Context;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;
import com.kamilstosik.hudl.map.MapManager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Test MapManager
 * Created by Kamil on 5/21/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class MapManagerTest {

    @Mock
    Context mMockContext;

    @Mock
    Geocoder mMockGeocoder;

    @Test
    public void getGeocoderTest() {
        MapManager.getInstance().getAndInitGeocoder(mMockContext);
    }

    @Test
    public void geocodeLocationToExtractCityTest() {
        LatLng pos = new LatLng(51.509865, -0.118092);
        MapManager.getInstance().geocodeLocationToExtractCity(mMockGeocoder, pos);
    }
}
